# LightShip Transcription Software

This project will leverage a neural network in order to obtain certain types of information from audio files. This information will include but is not limited to;

- A transcript of the conversation in the audio file
	- Including time stamps and participant recognition
- ML based understanding of elements like tone and emotion
- Angular front end web application
	- Uploading audio files for transcription
